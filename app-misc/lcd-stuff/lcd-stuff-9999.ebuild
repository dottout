# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-misc/lcd-stuff/lcd-stuff-0.1.6.ebuild,v 1.2 2009/11/09 13:20:57 maekke Exp $

EAPI=3
inherit git cmake-utils

DESCRIPTION="lcd-stuff is a client for lcdproc that displays RSS, Weather, MPD and new mail."
HOMEPAGE="http://lcd-stuff.berlios.de/"
EGIT_REPO_URI="git://git.berlios.de/lcd-stuff"
EGIT_PROJECT="lcd-stuff"

KEYWORDS="amd64 x86"
SLOT="0"
LICENSE="GPL-2" # and GPL-2 only

RDEPEND="app-misc/lcdproc
	net-misc/curl
	dev-libs/glib
	mail? ( net-libs/libetpan )
	mpd? ( >=media-libs/libmpd-0.12.0 )
	weather? ( net-libs/libnxml )
	rss? ( net-libs/libmrss net-libs/libnxml )"
DEPEND="${DEPEND}
	dev-util/pkgconfig"

IUSE="mail mpd weather rss"

src_unpack() {
	git_src_unpack
}


src_configure() {
	mycmakeargs=(
		$(cmake-utils_use_enable mail MAIL)
		$(cmake-utils_use_enable mpd MPD)
		$(cmake-utils_use_enable weather WEATHER)
		$(cmake-utils_use_enable rss RSS)
	)
	cmake-utils_src_configure

}

src_install() {

	cmake-utils_src_install

	newconfd "${FILESDIR}/${PN}.confd" ${PN}
	newinitd "${FILESDIR}/${PN}-0.1.2-r1.initd" ${PN}

	dodoc ChangeLog README
}
