# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit subversion eutils

ESVN_REPO_URI="https://amsn.svn.sourceforge.net/svnroot/amsn/trunk/amsn"
ESVN_PROJECT="amsn"

DESCRIPTION="Alvaro's Messenger client for MSN"
HOMEPAGE="http://amsn.sourceforge.net"

IUSE="debug nolibng gnome kde skins plugins"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"

DEPEND=">=dev-lang/tcl-8.5.0
	>=dev-lang/tk-8.5.0
	>=dev-tcltk/tls-1.5.0
        media-libs/jpeg
        media-libs/libpng
        >=dev-tcltk/snack-2.2.10
        >=net-libs/gupnp-igd-0.1.3
        media-libs/libv4l
	net-libs/telepathy-farsight
	>=media-libs/gst-plugins-base-0.10.23
	>=media-libs/gstreamer-0.10.23
	>=net-libs/farsight2-0.0.14
	media-libs/gst-plugins-bad"

pkg_setup() {
	ewarn "This is a LIVE SVN ebuild."
	ewarn "That means there are NO promises it will work."
}

src_configure() {
        econf $(use_enable debug) || die "configure script failed"
}

src_install() {
        make DESTDIR="${D}" install || die
        dodoc AGREEMENT TODO README FAQ CREDITS

        domenu amsn.desktop
        sed -i -e s:.png:: "${D}/usr/share/applications/amsn.desktop"

        cd desktop-icons
        for i in *; do
                if [ -e ${i}/msn.png ]; then
                        insinto /usr/share/icons/hicolor/${i}/apps
                        doins  ${i}/msn.png
                fi
        done

	if use skins; then
		einfo "Downloading and installing skins from SVN"
		svn co https://amsn.svn.sourceforge.net/svnroot/amsn/trunk/amsn-extras/skins skins
		cp -r skins/* /usr/share/amsn_dev/skins/
	fi

	if use plugins; then
		einfo "Downloading and installing plugins from SVN"
		svn co https://amsn.svn.sourceforge.net/svnroot/amsn/trunk/amsn-extras/plugins plugins
		cp -r plugins/* /usr/share/amsn_dev/plugins/
	fi
}

pkg_postinst() {
	ewarn "You might have to remove ~/.amsn prior to running as user if amsn hangs on start-up."
	ewarn "Remember, this stuff is SVN only code so dont cry when"
	ewarn "I break you :)."
	ewarn "If you want to report bugs, go to our forum at http://amsn.sf.net/forums"
	ewarn "or use our IRC channel at irc.freenode.net #amsn"
	ewarn ""
	ewarn "In order to use this cvs version, run amsn_dev instead of amsn."
}

pkg_postrm() {
        fdo-mime_desktop_database_update
        gnome2_icon_cache_update
}

