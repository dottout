# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit git eutils

EGIT_REPO_URI="http://github.com/emesene/emesene.git"
EGIT_PROJECT="emesene"
DESCRIPTION="Platform independent MSN Messenger client written in Python+GTK"
HOMEPAGE="http://www.emesene.org"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~alpha ~amd64 ~hppa ~ppc ~sparc ~x86"


DEPEND="
	>=dev-lang/python-2.5
	>=x11-libs/gtk+-2.8.20
	>=dev-python/pygtk-2.12
	dev-python/pydns
	>=dev-python/papyon-0.4.8	
	dev-python/xmpppy"

RDEPEND="${DEPEND}"

src_unpack() {
        git_src_unpack
}

src_compile() {

python setup.py build build_ext -i

}

src_install() {
	python setup.py install
	dodir /usr/share/emesene
	insinto /usr/share/emesene
	doins -r ./*
	dodir /usr/bin
	dosym /usr/share/emesene/build/lib/emesene/emesene /usr/bin
	exeinto /usr/share/emesene
	doexe emesene
	newicon ${S}/themes/default/icon96.png ${PN}.png
	make_desktop_entry emesene "EmeSeNe" ${PN}.png
}

