# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit git cmake-utils

DESCRIPTION="Wicd client build on the KDE Development Platform"
HOMEPAGE="http://kde-apps.org/content/show.php/Wicd+Client+KDE?content=132366"
EGIT_REPO_URI="git://gitorious.org/wicd-client-kde/wicd-client-kde.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND=">=kde-base/kdelibs-4
        >=x11-libs/qt-gui-4.5.0
        >=x11-libs/qt-dbus-4.5.0
        >=x11-libs/qt-core-4.5.0
        net-misc/wicd"
DEPEND="${RDEPEND}"

src_unpack() {
        git_src_unpack
}
