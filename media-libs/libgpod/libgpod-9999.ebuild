## Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit git eutils autotools

EGIT_REPO_URI="git://gtkpod.git.sourceforge.net/gitroot/gtkpod/libgpod"
DESCRIPTION="iPod library"
HOMEPAGE="http://www.gtkpod.org/libgpod.html"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="gtk python doc test"

RDEPEND=">=dev-libs/glib-2.4
	sys-apps/hal
	sys-apps/sg3_utils
	gtk? ( >=x11-libs/gtk+-2 )
	python? ( >=dev-lang/python-2.3
	>=dev-lang/swig-1.3.24
	>=x11-libs/gtk+-2
	media-libs/mutagen
	gtk? ( >=dev-python/pygobject-2 ) )
	test? ( media-libs/taglib )"
DEPEND="${RDEPEND}
	doc? ( dev-util/gtk-doc )
	dev-util/pkgconfig"

pkg_setup() {
	ewarn "This is a LIVE SVN ebuild."
	ewarn "That means there are NO promises it will work."
}


src_compile() {
	./autogen.sh

	local myconf

	if use gtk; then
	myconf="--enable-pygobject"
	else
	myconf="--disable-pygobject"
	fi

	econf ${myconf} \
		$(use_enable doc gtk-doc) \
		$(use_enable gtk gdk-pixbuf) \
		$(use_with python) || die "configure failed"
	emake || die "make failed"

}

src_install() {
	make DESTDIR="${D}" install || die "install failed"
	dodoc README TROUBLESHOOTING AUTHORS NEWS README.SysInfo
}

pkg_postinst() {
	einfo "If you use a newer Ipod like the Ipod classic, make sure to read"
	einfo "the information in README.SysInfo in /usr/share/doc/${P}"
	einfo "This is no needed if HAL is installed and works correctly."
}
