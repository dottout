# Copyright 1999-2009 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2

inherit rpm

DESCRIPTION="HSO UMTS PyGTK Connection Manager"
HOMEPAGE="http://www.pharscape.org/"
SRC_URI="http://www.pharscape.org/Downloads.html/RPM/hsoconnect-py2.5-1.1.128-2.noarch.rpm"
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"

RDEPEND="=dev-lang/python-2.5*
	net-dialup/hsolink
	sys-apps/usb_modeswitch
	sys-fs/udev"

src_install() {
	chmod -R go-w "${WORKDIR}/usr/"
	chgrp -R uucp "${WORKDIR}/usr/share/HSOconnect/"
	chmod g+sw "${WORKDIR}/usr/share/HSOconnect/"
	chmod -R g+w "${WORKDIR}/usr/share/HSOconnect/HSOconnect.cfg"
	chmod -R g+w "${WORKDIR}/usr/share/HSOconnect/profiles/"
	chmod g+s "${WORKDIR}"/usr/share/HSOconnect/profiles/*/
	mv "${WORKDIR}"/* "${D}"

	insinto /etc/udev/rules.d/
	doins "${FILESDIR}"/91-usb_modeswitch.rules

	echo 'CONFIG_PROTECT="/usr/share/HSOconnect"' > "${T}/50${PN}"
	echo 'CONFIG_PROTECT_MASK="/usr/share/HSOconnect/images /usr/share/HSOconnect/languages"' >> "${T}/50${PN}"
	doenvd "${T}/50${PN}"
}

