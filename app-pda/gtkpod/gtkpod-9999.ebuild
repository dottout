# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit git eutils

EGIT_REPO_URI="git://gtkpod.git.sourceforge.net/gitroot/gtkpod/gtkpod"
EGIT_BOOTSTRAP="./autogen.sh"

DESCRIPTION="GUI for iPod using GTK2"
HOMEPAGE="http://gtkpod.sourceforge.net"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="aac flac gnome hal ogg mp3"

RDEPEND=">=x11-libs/gtk+-2.8
        >=media-libs/libid3tag-0.15
        >=gnome-base/libglade-2.4
        >=gnome-base/libgnomecanvas-2.14
        >=media-libs/libgpod-0.7
        >=net-misc/curl-7.10
        >=dev-libs/glib-2.16.0
        mp3? ( media-sound/lame )
        aac? ( media-libs/libmp4v2 )
        ogg? ( media-libs/libvorbis
                media-sound/vorbis-tools )
        flac? ( media-libs/flac )"
DEPEND="${RDEPEND}
        dev-util/pkgconfig
        sys-devel/flex
	dev-util/anjuta"

pkg_setup() {
	ewarn "This is a LIVE SVN ebuild."
	ewarn "That means there are NO promises it will work."
}

src_compile() {
	econf \
		$(use_with ogg) \
		$(use_with flac) \
                $(use_enable nls) 
}

src_install() {
        emake DESTDIR="${D}" install || die
        dodoc AUTHORS ChangeLog NEWS README TROUBLESHOOTING *.txt
}
